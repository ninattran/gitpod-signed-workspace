FROM gitpod/workspace-full:latest
MAINTAINER Cody Neiman <cody.neiman@yale.edu>

RUN sudo apt update && sudo apt upgrade -yq && sudo apt -y install valgrind
